document.addEventListener("DOMContentLoaded", function() {
    var moreButtons = document.getElementsByClassName("showmore_button");
    for (var i = 0; i < moreButtons.length; i++) {
        moreButtons[i].addEventListener("click", function() {
            var row = this.closest("tr");
            var toToggle = row.querySelectorAll(".showmore_cut");
            var content = this.innerHTML;
            for (var j = 0; j < toToggle.length; j++) {
                var current = toToggle[j].style.display;
                console.log(current);
                if (current == "none" || current == "") {
                    toToggle[j].style.display = "block";
                } else {
                    toToggle[j].style.display = "none";
                }
            };
            if (content.includes("↓")) {
                this.innerHTML = content.replaceAll("↓", "↑");
            } else {
                this.innerHTML = content.replaceAll("↑", "↓");
            }
        });
    }
});