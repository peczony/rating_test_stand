#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import functools
from collections import defaultdict, Counter
import pyexcel
import scipy.stats
import scipy.special
import numpy as np
import os
from trueskill import Rating, rate_1vs1
from counter import RatingCounter, parse_date, tryfloat

CLASSNAME = "NaiveTrueskill"


class HashableRating(Rating):
    def __hash__(self):
        return hash((self.mu, self.sigma))


@functools.lru_cache(maxsize=None)
def cache_rate_1vs1(*args, **kwargs):
    rating = rate_1vs1(*args, **kwargs)
    return (HashableRating(rating[0]), HashableRating(rating[1]))


def parse_date(date_):
    return datetime.datetime.strptime(date_, "%Y-%m-%d %H:%M:%S").date()


class NaiveTrueskill(RatingCounter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.state["ratings"] = defaultdict(HashableRating)
        self.state["releases"] = defaultdict(dict)
        self.state["releases_teams"] = defaultdict(list)
        self.state["deltas"] = defaultdict(list)
        self.file_prefix = f"naive_trueskill_{self.ts}_"
        self.comparison = []

    @staticmethod
    def get_rating_value(tsrating):
        return tsrating.mu - 3 * tsrating.sigma

    @staticmethod
    def calc_games(games_counter, threshold):
        result = 0
        for date in games_counter:
            if date < threshold:
                continue
            result += games_counter[date]
        return result

    def is_eligible(self, idteam, target_date):
        games_last_period = self.calc_games(
            self.games_count[idteam],
            target_date - datetime.timedelta(days=self.config["base_threshold"]),
        )
        has_base = (
            self.last_base.get(idteam)
            and (target_date - self.last_base[idteam]).days
            < self.config["base_threshold"]
        )
        base_special_case = int(idteam) in (49804, 670)  # БК, Ксеп
        return (has_base or base_special_case) and games_last_period > self.config[
            "games_threshold"
        ]

    def rate(self, _, results, frozen_ratings):
        for _, res in enumerate(results):
            won_against_team = [
                r for r in results if float(r["position"]) < float(res["position"])
            ][-10:]
            lost_against_team = [
                r for r in results if float(r["position"]) > float(res["position"])
            ][:10]
            for t in won_against_team:
                _, self.state["ratings"][res["idteam"]] = cache_rate_1vs1(
                    frozen_ratings[t["idteam"]], self.state["ratings"][res["idteam"]]
                )
            for t in lost_against_team:
                self.state["ratings"][res["idteam"]], _ = cache_rate_1vs1(
                    self.state["ratings"][res["idteam"]], frozen_ratings[t["idteam"]]
                )

    @staticmethod
    def count_total_delta(deltas):
        if not deltas:
            return 0
        return sum(x["delta"] for x in deltas)

    def save_ratings(self):
        target_date = self.current_date + datetime.timedelta(self.step_days)
        top = sorted(
            [x for x in self.state["ratings"] if self.is_eligible(x, target_date)],
            key=lambda x: self.get_rating_value(self.state["ratings"][x]),
            reverse=True,
        )
        ranks = scipy.stats.rankdata(
            [-self.get_rating_value(self.state["ratings"][x]) for x in top]
        )
        ranks_dict = dict(zip(top, ranks))
        dest_file_path = os.path.join(self.output_dir, self.file_prefix + str(target_date) + ".xlsx")
        print(f"saving {dest_file_path}...")
        result_array = [["id", "name", "mu", "sigma", "rank"]]
        data_for_rating_page = []
        for i, t in enumerate(top):
            deltas = self.state["deltas"].get(t) or []
            data_for_rating = {
                "id": t,
                "name": self.state["names"].get(t) or "-",
                "deltas": deltas,
                "total_delta": self.count_total_delta(deltas),
                "position": ranks_dict[t],
                "prev_position": self.prev_position.get(t),
                "rating": self.get_rating_value(self.state["ratings"][t]),
                "release_date": target_date,
            }
            self.state["releases_teams"][t].append(data_for_rating)
            data_for_rating_page.append(data_for_rating)
            self.state["deltas"][t] = []
            if i > self.config["top_n_teams"]:
                continue
            result_array.append(
                [
                    t,
                    self.state["names"].get(t) or "-",
                    self.state["ratings"][t].mu,
                    self.state["ratings"][t].sigma,
                    self.get_rating_value(self.state["ratings"][t]),
                ]
            )
        self.prev_position = ranks_dict
        self.generate_release_page(data_for_rating_page)
        if target_date >= self.end_date:
            self.generate_teams_pages()
            self.calc_benchmarks_and_save()
        pyexcel.save_as(array=result_array, dest_file_name=dest_file_path)
