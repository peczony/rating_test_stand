import datetime
import os
import math
import json
from collections import defaultdict, Counter
from scipy.stats.stats import RepeatedResults
import yaml
import numpy as np
import scipy.stats
import pyexcel


TOML_HEADER = """+++
date = {date}
title = "{title}"
+++

"""


def parse_date(date_):
    return datetime.datetime.strptime(date_, "%Y-%m-%d %H:%M:%S").date()


def tryfloat(x):
    try:
        x = float(x)
        if np.isfinite(x):
            return x
    except (ValueError, TypeError):
        return


def escape(name):
    return name.replace('"', '\\"')


RELEASE_TABLE_HEADER = """<table class="tf">
<thead>
<tr>
<th class="small_table_header"><!-- дельта места--></th> 
<th class="small_table_header">М</th>
<th class="small_table_header"><!-- дельта рейтинга--></th>
<th class="small_table_header">Р</th>
<th class="big_table_header">Название</th>
<th class="small_table_header"><!-- дельта рейтинга на турнире--></th>
<th class="big_table_header">Турниры</th>
<th class="medium_table_header"><!-- ещё--></th>
</tr>
</thead><tbody>
"""

RELEASE_ROW = """
<tr>
<td class="right">{position_delta}</td>
<td class="right">{position}</td>
<td><span class="good right">{total_delta}</span></td>
<td class="right">{rating}</td>
<td><a href="/teams/{id_}">{name}</a></td>
<td>{tournament_deltas}</td>
<td>{tournaments}</td>
<td>{showmore}</td>
</tr>
"""


class RatingCounter:
    def __init__(self, tournaments, config_path):
        self.tournaments = tournaments
        self.config_path = config_path
        with open(config_path, encoding="utf8") as f:
            self.config = yaml.safe_load(f)
        self.output_dir = self.resolve_dir(self.config["output"])
        self.start_date = self.config["start_date"]
        self.end_date = self.config["end_date"]
        self.step_days = self.config["step_days"]
        self.state = {"ratings": {}, "names": {}}
        self.ts = datetime.datetime.now().strftime("%s")
        self.current_date = self.start_date
        self.benchmark_data = []
        self.games_count = defaultdict(Counter)
        self.last_base = {}
        self.prev_position = {}
        if "hugo_output" in self.config:
            self.hugo_output_dir = self.resolve_dir(self.config["hugo_output"])

    def resolve_dir(self, dir_):
        config_dir = os.path.dirname(self.config_path)
        if os.path.isabs(dir_):
            return dir_
        return os.path.abspath(os.path.join(config_dir, dir_))

    def get_cohort_coeff(self, team_id):
        if team_id not in self.prev_position:
            return
        pp = self.prev_position[team_id]
        if pp <= 25:
            return 1.5
        elif pp <= 50:
            return 1.4
        elif pp <= 50:
            return 1.4
        elif pp <= 100:
            return 1.3
        elif pp <= 200:
            return 1.18
        elif pp <= 300:
            return 1.12
        elif pp <= 500:
            return 1
        elif pp <= 700:
            return 0.9
        elif pp <= 900:
            return 0.8
        elif pp <= 1000:
            return 0.75
        return 0.65

    @staticmethod
    def wrap_position(position):
        if position == int(position):
            return int(position)
        return position

    @staticmethod
    def wrap_dl(dl):
        if dl and isinstance(dl, (int, float)):
            return round(dl, 2)
        return "-"

    def generate_tournament_page(self, tournament, results, overwrite=False):
        if "hugo_output" not in self.config:
            return
        if self.config.get("overwrite"):
            overwrite = True
        tournament_dir = os.path.join(self.hugo_output_dir, "tournaments")
        if not os.path.exists(tournament_dir):
            os.makedirs(tournament_dir)
        tournament_file = os.path.join(
            tournament_dir, f"{tournament['idtournament']}.md"
        )
        if os.path.exists(tournament_file) and not overwrite:
            return
        md_output = [
            TOML_HEADER.format(
                date=str(datetime.date.today()), title=escape(tournament["name"])
            )
        ]
        md_output.extend(
            [
                "",
                f"DL: {self.wrap_dl(tournament['ts_extra']['dl'])}, rawDL: {self.wrap_dl(tournament['ts_extra']['raw_dl'])}",
                "",
            ]
        )
        md_output.append(
            "| Место | Место (П) | ID | Название | Взято | Рейтинг | Дельта | DL |"
        )
        md_output.append("| --- | --- | --- | --- | --- | --- | --- | --- |")
        for res in results:
            md_output.append(
                f"| {self.wrap_position(res['position'])} | {self.wrap_position(res['predicted_position'])} | {res['id']} | [{res['name']}](/teams/{res['id']}) | {res['points']} | {round(res['rating'], 2)} | {round(res['delta'], 2)} | {self.wrap_dl(res.get('dl'))} |"
            )
        with open(tournament_file, "w") as f:
            f.write("\n".join(md_output))

    def validate_tournament(self, tournament):
        result = True
        for res in tournament["results"]:
            if len(res.get("mask") or "") != int(
                tournament["extended_metadata"][0].get("questions_total") or "12345"
            ):
                result = False
                break
        return result

    @staticmethod
    def calc_raw_dl(q, qt):
        return round(10.0 * (1 - q / qt), 2)

    def process_tournament(self, tournament):
        results = sorted(
            tournament["results"],
            key=lambda x: (float(x["position"]), int(x["idteam"])),
        )
        predicted_positions = scipy.stats.rankdata(
            [
                -self.get_rating_value(self.state["ratings"][x["idteam"]])
                for x in results
            ]
        )
        if self.config.get("benchmarks"):
            self.prepare_benchmark_data(tournament, results, predicted_positions)
        for team in tournament["recaps"]:
            if any([x["is_base"] == "1" for x in team["recaps"]]):
                self.last_base[team["idteam"]] = parse_date(tournament["date_end"])
        if not self.validate_tournament(tournament):
            return

        frozen_ratings = self.state["ratings"].copy()
        results_for_page = []
        self.rate(tournament, results, frozen_ratings)
        for i, res in enumerate(results):
            self.games_count[res["idteam"]][parse_date(tournament["date_end"])] += 1
            self.state["names"][res["idteam"]] = res["base_name"]
            delta = self.get_rating_value(
                self.state["ratings"][res["idteam"]]
            ) - self.get_rating_value(frozen_ratings[res["idteam"]])
            raw_dl = self.calc_raw_dl(
                int(res["questions_total"]),
                int(tournament["extended_metadata"][0]["questions_total"]),
            )
            cohort_coeff = self.get_cohort_coeff(res["idteam"])
            if cohort_coeff:
                corrected_dl = raw_dl * cohort_coeff
            else:
                corrected_dl = None

            results_for_page.append(
                {
                    "id": res["idteam"],
                    "name": res["current_name"],
                    "points": int(res["questions_total"]),
                    "position": float(res["position"]),
                    "predicted_position": predicted_positions[i],
                    "rating": self.get_rating_value(frozen_ratings[res["idteam"]]),
                    "delta": delta,
                    "raw_dl": raw_dl,
                    "dl": corrected_dl,
                }
            )
            self.state["deltas"][res["idteam"]].append(
                {
                    "tournament_id": tournament["idtournament"],
                    "tournament_name": tournament["name"],
                    "delta": delta,
                }
            )
        raw_dl = [x["raw_dl"] for x in results_for_page if x["raw_dl"]]
        corrected_dl = [x["dl"] for x in results_for_page if x["dl"]]
        tournament["ts_extra"] = {
            "raw_dl": None if len(raw_dl) < 10 else round(np.median(raw_dl), 2),
            "dl": None if len(corrected_dl) < 10 else round(np.median(corrected_dl), 2),
        }
        self.generate_tournament_page(tournament, results_for_page)

    def format_position_delta(self, prev_position, position):
        if not prev_position:
            return "NEW"
        delta = prev_position - position
        if delta > 0:
            return f"+{self.wrap_position(delta)}"
        elif delta < 0:
            return f"−{self.wrap_position(abs(delta))}"
        else:
            return "–"

    # @staticmethod
    # def format_included_tournaments(deltas):
    #     if not deltas:
    #         return "-"
    #     formatted = [
    #         f"{d['tournament_id']} [{d['tournament_name']}](/tournaments/{d['tournament_id']}) ({round(d['delta'], 2)})"
    #         for d in deltas
    #     ]
    #     return " <br/> ".join(formatted)

    @staticmethod
    def format_single_delta(delta):
        if not delta:
            return '<span class="neutral">0</span>'
        if delta > 0:
            sign = "+"
            class_ = "good"
        else:
            sign = "−"
            class_ = "bad"
        return f'<span class="{class_}">{sign}{round(abs(delta), 2)}</span>'

    def format_tournament_deltas(self, deltas):
        if not deltas:
            return "–"
        result = self.format_single_delta(deltas[0]["delta"])
        if len(deltas) > 1:
            result += (
                '<div class="showmore_cut">'
                + "<br/>\n".join(
                    [self.format_single_delta(x["delta"]) for x in deltas[1:]]
                )
                + "</div>"
            )
        return result

    @staticmethod
    def format_single_tournament(delta):
        return '<a href="/tournaments/{tournament_id}">{tournament_name}</a>'.format(
            **delta
        )

    def format_included_tournaments(self, deltas):
        if not deltas:
            return "–"
        result = self.format_single_tournament(deltas[0])
        if len(deltas) > 1:
            result += (
                '<div class="showmore_cut">'
                + "<br/>\n".join([self.format_single_tournament(x) for x in deltas[1:]])
                + "</div>"
            )
        return result

    def generate_release_page(self, release_data, overwrite=False):
        if "hugo_output" not in self.config:
            return
        if not release_data:
            return
        if self.config.get("overwrite"):
            overwrite = True
        target_dir = os.path.join(self.hugo_output_dir, "releases")
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        release_date = release_data[0]["release_date"]
        target_file = os.path.join(target_dir, f"{release_date}.md")
        if os.path.exists(target_file) and not overwrite:
            return
        md_output = [
            TOML_HEADER.format(
                date=str(datetime.date.today()),
                title=escape(f"Релиз от {release_date}"),
            )
        ]
        md_output.append(RELEASE_TABLE_HEADER)
        for res in release_data:
            position_delta = self.format_position_delta(
                res["prev_position"], res["position"]
            )
            if len(res["deltas"]) > 1:
                showmore = """<span class="showmore_button">ещё {n} ↓</span>""".format(
                    n=len(res["deltas"]) - 1
                )
            else:
                showmore = ""
            md_output.append(
                RELEASE_ROW.format(
                    position_delta=position_delta,
                    position=res["position"],
                    total_delta=res["total_delta"],
                    rating=round(res["rating"], 2),
                    id_=res["id"],
                    name=res["name"],
                    tournament_deltas=self.format_tournament_deltas(res["deltas"]),
                    tournaments=self.format_included_tournaments(res["deltas"]),
                    showmore=showmore,
                )
            )
        md_output.append("</tbody></table>")
        with open(target_file, "w") as f:
            f.write("\n".join(md_output))

    def generate_teams_pages(self, overwrite=False):
        if "hugo_output" not in self.config:
            return
        if self.config.get("overwrite"):
            overwrite = True
        target_dir = os.path.join(self.hugo_output_dir, "teams")
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        for id_ in self.state["releases_teams"]:
            target_file = os.path.join(target_dir, f"{id_}.md")
            name = self.state["names"].get(id_) or "–"
            if os.path.exists(target_file) and not overwrite:
                return
            md_output = [
                TOML_HEADER.format(
                    date=str(datetime.date.today()), title=escape(f"{id_} {name}")
                )
            ]
            md_output.append(
                "| Дата релиза | Место | Рейтинг | Дельта | Учтённые турниры"
            )
            md_output.append("| --- | --- | --- | --- | --- |")
            for res in self.state["releases_teams"][id_][::-1]:
                included_tournaments = self.format_included_tournaments(res["deltas"])
                date = res["release_date"]
                md_output.append(
                    f"| [{date}](/releases/{date}) | {self.wrap_position(res['position'])} | {round(res['rating'], 2)} | {round(res['total_delta'], 2)} | {included_tournaments} "
                )
            with open(target_file, "w") as f:
                f.write("\n".join(md_output))

    def save_ratings(self):
        pass

    @staticmethod
    def count_pearsonr(l1, actual):
        assert len(l1) == len(actual)
        return abs(scipy.stats.pearsonr(l1, actual)[0])

    @staticmethod
    def count_ndcg(l1, actual):
        assert len(l1) == len(actual)
        len_ = len(l1)
        max_ndcg = 0
        ndcg = 0
        for pair in zip(l1, actual):
            diff = abs(pair[0] - pair[1])
            rel = 1.0 - diff / len_
            assert rel <= 1.0
            ndcg += rel / math.log(pair[1] + 1, 2)
            max_ndcg += 1.0 / math.log(pair[1] + 1, 2)
        return ndcg / max_ndcg

    def prepare_benchmark_data(self, tournament, results, predicted_positions):
        zipped_positions = [
            {
                "predicted_baseline": tryfloat(x["predicted_position"]),
                "actual": tryfloat(x["position"]),
                "predicted": predicted_positions[i],
            }
            for i, x in enumerate(results)
        ]
        clean = [
            x
            for x in zipped_positions
            if x["actual"] is not None
            and x["predicted"] is not None
            and x["predicted_baseline"] is not None
        ]
        result = {
            "tournament_id": tournament["idtournament"],
            "tournament_name": tournament["name"],
            "date_end": tournament["date_end"],
        }
        baseline = [x["predicted_baseline"] for x in clean]
        test = [x["predicted"] for x in clean]
        actual = [x["actual"] for x in clean]
        if len(actual) < 15:
            return
        if len(set(test)) == 1 or len(set(actual)) == 1:
            return
        np.seterr(all="raise")
        for benchmark in self.config.get("benchmarks") or []:
            func = getattr(self, f"count_{benchmark}")
            try:
                baseline_val = func(baseline, actual)
                test_val = func(test, actual)
            except:
                import pdb

                pdb.set_trace()
            result[benchmark] = {"baseline": baseline_val, "test": test_val}
        self.benchmark_data.append(result)

    @staticmethod
    def mw_wrapper(l1, l2):
        test_up = scipy.stats.mannwhitneyu(l1, l2, alternative="less")
        test_down = scipy.stats.mannwhitneyu(l1, l2, alternative="greater")
        if test_up[1] < test_down[1] and test_up[1] < 0.05:
            return f"↑ ({test_up[1]:.02})"
        elif test_up[1] > test_down[1] and test_down[1] < 0.05:
            return f"↓ ({test_up[1]:.02})"
        return "no differences"

    def calc_benchmarks(self, batch, batch_number, final_benchmarks):
        for benchmark in self.config.get("benchmarks") or []:
            baseline = [x[benchmark]["baseline"] for x in batch]
            test = [x[benchmark]["test"] for x in batch]
            mw = self.mw_wrapper(baseline, test)
            final_benchmarks.append(
                {
                    "batch_number": batch_number,
                    "benchmark": benchmark,
                    "median_baseline": round(np.median(baseline), 2),
                    "median_test": round(np.median(test), 2),
                    "mw_result": mw,
                }
            )

    def _generate_xlsx_array_tournaments(self):
        header_row = ["date_end", "id", "name"]
        result = [header_row]
        benchmarks = self.config.get("benchmarks") or []
        for b in benchmarks:
            header_row.extend([f"{b}_baseline", f"{b}_test"])
        for t in self.benchmark_data:
            row = [t["date_end"], t["tournament_id"], t["tournament_name"]]
            for b in benchmarks:
                row.extend([round(t[b]["baseline"], 2), round(t[b]["test"], 2)])
            result.append(row)
        return result

    def _generate_xlsx_array_benchmarks(self):
        header_row = [
            "batch_number",
            "benchmark",
            "median_baseline",
            "median_test",
            "mw_result",
        ]
        result = [header_row]
        for benchmark in self.final_benchmarks:
            row = [benchmark[k] for k in header_row]
            result.append(row)
        return result

    def save_benchmarks_xlsx(self):
        bookdict = {
            "tournaments_data": self._generate_xlsx_array_tournaments(),
            "benchmarks": self._generate_xlsx_array_benchmarks(),
        }
        benchmarks_xlsx_output = os.path.join(
            self.output_dir, f"benchmarks_{self.ts}.xlsx"
        )
        pyexcel.save_book_as(bookdict=bookdict, dest_file_name=benchmarks_xlsx_output)

    def calc_benchmarks_and_save(self):
        bcount = self.config["benchmark_count"]
        batch_size = self.config["benchmark_size"]
        n = batch_size * bcount
        last_n_tournaments = self.benchmark_data[-n:]
        assert len(last_n_tournaments) == n
        final_benchmarks = []
        for i in range(bcount):
            batch, last_n_tournaments = (
                last_n_tournaments[:batch_size],
                last_n_tournaments[batch_size:],
            )
            self.calc_benchmarks(batch, i + 1, final_benchmarks)
        self.final_benchmarks = final_benchmarks
        benchmarks_json_output = os.path.join(
            self.output_dir, f"benchmarks_{self.ts}.json"
        )
        with open(benchmarks_json_output, "w", encoding="utf8") as f:
            f.write(
                json.dumps(
                    {
                        "benchmark_data": self.benchmark_data,
                        "final_benchmarks": self.final_benchmarks,
                    },
                    indent=2,
                    sort_keys=True,
                    ensure_ascii=False,
                )
            )
        self.save_benchmarks_xlsx()

    def check_tournament(self, t):
        return (
            t["extended_metadata"][0]["tournament_in_rating"] == "1"
            and parse_date(t["date_end"]) >= self.current_date
            and parse_date(t["date_end"])
            < (self.current_date + datetime.timedelta(days=self.step_days))
        )

    def make_next_release(self):
        tournaments = [x for x in self.tournaments if self.check_tournament(x)]
        for tournament in tournaments:
            self.process_tournament(tournament)
        self.save_ratings()

    def count_ratings(self):
        while self.current_date < self.end_date:
            self.make_next_release()
            self.current_date += datetime.timedelta(self.step_days)
